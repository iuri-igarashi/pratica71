/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

/**
 *
 * @author xadu
 */
public class JogadorComparator implements Comparator<Jogador> {

    boolean ordenaNumero, numeroAscendente, nomeAscendente;

    @Override
    public int compare(Jogador o1, Jogador o2) {

        int comparacao;

        if (ordenaNumero) {
            comparacao = comparanumero(o1, o2);
            if (comparacao == 0) {
                comparacao = comparanome(o1, o2);
            }
        } else {
            comparacao = comparanome(o1, o2);
            if (comparacao == 0) {
                comparacao = comparanumero(o1, o2);
            }
        }

        return comparacao;
    }

    public int comparanumero(Jogador o1, Jogador o2) {
        if (o1.numero < o2.numero) {
            if (numeroAscendente) {
                return -1;
            } else {
                return 1;
            }
        } else if (o1.numero > o2.numero) {
            if (numeroAscendente) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return 0;
        }
    }

    public int comparanome(Jogador o1, Jogador o2) {
        if (o1.nome.compareTo(o2.nome) < 0) {
            if (nomeAscendente) {
                return -1;
            } else {
                return 1;
            }
        } else if (o1.nome.compareTo(o2.nome) > 0) {
            if (nomeAscendente) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return 0;
        }
    }

    public JogadorComparator(boolean ordenaNumero, boolean numeroAscendente, boolean nomeAscendente) {
        this.ordenaNumero = ordenaNumero;
        this.numeroAscendente = numeroAscendente;
        this.nomeAscendente = nomeAscendente;
    }

    public JogadorComparator() {
        this.ordenaNumero = true;
        this.numeroAscendente = true;
        this.nomeAscendente = true;
    }
    
}
