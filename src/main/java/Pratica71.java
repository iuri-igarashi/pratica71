
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {
    public static void main(String[] args) {
        Time time1 = new Time();
        //Time time2 = new Time();
        ArrayList<Jogador> timeOrdenado; 
       
        Scanner scanner = new Scanner(System.in);
        int numeroJogadoresLidos;
        System.out.println("Digite o numero de jogadores que deseja incluir:");
        //while(numeroJogadoresLidos < 1){
        numeroJogadoresLidos = scanner.nextInt();
        //}
        System.out.println("deu");
        String jogadorNome;
        int jogadorNumero = -1;
        Jogador jogador;
        
        
        for(int i=0; i<numeroJogadoresLidos; i++){
            //nome do jogador
            scanner.reset();
            System.out.println("Digite o nome do jogador");
            jogadorNome = scanner.next();
            //numero do jogador
            scanner.reset();
            try{
                System.out.println("Digite o numero do jogador " + jogadorNome + ":");
                jogadorNumero = scanner.nextInt();
                
            }catch(InputMismatchException e){
                jogadorNumero = 0;
            }finally{

                //adiciona o jogador
                jogador = new Jogador(jogadorNumero, jogadorNome);
                time1.addJogador("Jogador", jogador);
            }
        }
        
        /*Jogador player = new Jogador(1,"Fulano");
        time1.addJogador("Goleiro", player);
        
        player = new Jogador(1,"João");
        time2.addJogador("Goleiro", player);
        
        player = new Jogador(4,"Ciclano");
        time1.addJogador("Lateral", player);
        
        player = new Jogador(7,"José");
        time2.addJogador("Lateral", player); 
        
        player = new Jogador(10,"Beltrano");
        time1.addJogador("Atacante", player);
        
        player = new Jogador(15,"Mário");
        time2.addJogador("Atacante", player);*/
        
        //ordenacao
        JogadorComparator jc = new JogadorComparator(true, true, false);
        timeOrdenado = (ArrayList<Jogador>) time1.ordena(jc);
        
        System.out.print("");
        for(Jogador j : timeOrdenado) {
            System.out.println(j);
        }
        
        //adicionar mais jogadores
        while(jogadorNumero != 0){
            //nome do jogador
            scanner.reset();
            System.out.println("Digite o nome do jogador");
            jogadorNome = scanner.next();
            //numero do jogador
            scanner.reset();
            try{
                System.out.println("Digite o numero do jogador " + jogadorNome + ":");
                jogadorNumero = scanner.nextInt();
                if(jogadorNumero == 0)
                    break;
                
            }catch(InputMismatchException e){
                jogadorNumero = 0;
            }finally{

                //adiciona o jogador
                jogador = new Jogador(jogadorNumero, jogadorNome);
                time1.addJogador("Jogador", jogador);
            }
        }
        
    }
}
